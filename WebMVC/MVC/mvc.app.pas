unit MVC.App;

{$mode ObjFPC}{$H+}

interface

uses
  {$IFDEF UNIX} cthreads, cmem, {$ENDIF}
  Classes, SysUtils, mvc.HttpServer, mvc.Config, mvc.DB;

type
  TCmd = class(TThread)
  private
    function Command: boolean;
  protected
    procedure Execute; override;
  public
  end;

  TMVCApp = class
  public
    procedure Run();
    procedure Close();
  end;

var
  MVCApp: TMVCApp;

implementation

{ TCmd }

function TCmd.Command: boolean;
var
  LResponse: string;
begin
  Writeln('input ''q'' Close Server');
  readln(LResponse);
  if LResponse.ToLower = 'q' then
    Result := False
  else
    Result := True;
end;

procedure TCmd.Execute;
begin

  Writeln('Server Start');
  Writeln('http://localhost:' + Config.Port);
  while True do
  begin
    if not Command then
      Break;
  end;
  MVCApp.Close();
end;

{ TMVC }

procedure TMVCApp.Run;
begin

  TCmd.Create(False).Start;
  TMVCServer.Start;
end;

procedure TMVCApp.Close;
begin
  TMVCServer.Stop;
end;

initialization
  MVCApp := TMVCApp.Create;

finalization
  MVCApp.Free;
end.
