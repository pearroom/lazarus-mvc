unit IndexController;

{$mode ObjFPC}{$H+}{$M+}

interface

uses
  Classes, SysUtils, BaseController, MVC.JSON, MVC.DataSet, fpjson,
  jsonparser;

type

  { TIndexController }

  TIndexController = class(TBaseController)
  published
    procedure index;
    procedure getdata;
    procedure getjsondata;
    procedure save;
    function Intercept: boolean; override;
  end;

implementation

{ TIndexController }
procedure TIndexController.save;
begin
  SetAttr('message', '你好');

end;

procedure TIndexController.index;
begin
  SetAttr('message', '你好');
  Session.setValue('name', '你好');
  Show('index');
end;

procedure TIndexController.getdata;
var
  res: IJObject;
  json, json2: IDataSet;

begin
  res := IIJObject();
  //  db2.DelByKey('users','id','11');
  with db2.Edit('users', 'id', '12') do
  begin
    S['name'] := '刘德华';
    post;
    ApplyUpdates;
  end;
  with db2.Add('users') do
  begin
    s['name'] := '刘德华';
    S['sex'] := '123';
    I['age'] := 12;
    post;
    ApplyUpdates;
  end;
  db2.Commit;
  json := db2.Find('select * from users');

  // json2 := db1.Find('select * from users');

  ShowJSON(json);
end;

procedure TIndexController.getjsondata;
var
  json: IJObject;
begin
  json := InputToJSON;
  ShowJSON(json);
end;

function TIndexController.Intercept: boolean;
begin
  Result := False;
end;

initialization
  SetRoute('', TIndexController);

end.
