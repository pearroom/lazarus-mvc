unit BaseController;

{$mode ObjFPC}{$H+}
interface

uses
  Classes, SysUtils, mvc.Route, MVC.controller, mvc.JSON, fpjson,
  mvc.Session, mvc.dbSQLite, mvc.dbMySql56;

type

  { TBaseController }

  TBaseController = class(TController)
  published
    function Intercept: boolean; override;
    procedure CreateController; override;
  public
    db2: TMySQL56DB;  {链接mysql数据库}
    db1: TSQLiteDB;  {链接sqlite数据库}
    destructor Destroy; override;
  end;

procedure SetRoute(routeUrl: string; Action: TClass; tplPath: string = '');

implementation

{ TBaseController }
procedure SetRoute(routeUrl: string; Action: TClass; tplPath: string);
begin
  Route.SetRoute(routeUrl, Action, tplPath);
end;

function TBaseController.Intercept: boolean;
var
  username: string;
  json: IJObject;
begin
  json := Session.getJSON_('user');
  username := Session.getValue('name');
  if json <> nil then
    Result := False
  else
    Result := True;
end;

procedure TBaseController.CreateController;
begin
  db2 := TMySQL56DB.Create('db2');
  db1 := TSQLiteDB.Create('db1');
  inherited CreateController;

end;

destructor TBaseController.Destroy;
begin
  db2.Free;
  db1.Free;
  inherited Destroy;
end;

end.
