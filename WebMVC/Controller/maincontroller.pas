unit MainController;

{$mode ObjFPC}{$H+}
{$M+}
interface

uses
  Classes, SysUtils, BaseController;

type

  { TMainController }

  TMainController = class(TBaseController)
  published
    procedure index;
    procedure main;
  end;

implementation

{ TMainController }

procedure TMainController.index;
begin
  ShowText('index');
end;

procedure TMainController.main;
begin
  ShowText('main');
end;

initialization
  SetRoute('main', TMainController, 'main');
end.
